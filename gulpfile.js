const gulp = require('gulp');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const combine = require('stream-combiner2').obj;
const del = require('del');

gulp.task('clean', () => del('./dist'));

gulp.task('styles', () =>
  combine(
    gulp.src('./assets/styles/index.sass'),
    sass({
      paths: ['./assets/styles'],
    }),
    postcss([
      autoprefixer({ browsers: ['last 5 versions', 'ie 10-11'] }),
    ]),
    concat('index.css'),
    gulp.dest('./dist/styles')
  ).on('error', sass.logError)
);

gulp.task('html', () =>
  gulp.src('./index.html')
    .pipe(gulp.dest('./dist'))
);

gulp.task('build', ['clean', 'styles', 'html']);
